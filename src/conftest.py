# conftest.py
import pytest
import json

def pytest_sessionstart(session):
    session.results = dict()

@pytest.hookimpl(tryfirst=True, hookwrapper=True)
def pytest_runtest_makereport(item, call):
    outcome = yield
    result = outcome.get_result()
    if result.when == 'call':
        item.session.results[result.nodeid] = result

# @pytest.hookimpl(trylast=True)
def pytest_sessionfinish(session: pytest.Session, exitstatus: pytest.ExitCode):
    requirement_fulfilled = {}
    for test,result in session.results.items():
        requirement_id = dict(result.user_properties).get('gitlab_requirement')
        requirement_fulfilled[requirement_id] = result.outcome
    with open('requirements.json', 'w') as r_fp:
        json.dump(requirement_fulfilled, r_fp)