import pytest
from loguru import logger
class TestPasses():
    def test_passes(self,record_property, caplog):
        logger.info('I pass')
        record_property("gitlab_requirement", "1")
        assert True
        
    def test_fails(self, record_property, caplog):
        logger.info('I fail')
        record_property("gitlab_requirement", "2")
        assert False

    @pytest.mark.skip(reason="Check to see what happens when skipped")
    def test_skip(self, record_property, caplog):
        logger.info('I failed')
        record_property("gitlab_requirement", "3")
        assert True
    
    @pytest.mark.xfail
    def test_xfail_fails(self, record_property, caplog):
        logger.info('I failed and I know it')
        record_property("gitlab_requirement", "4")
        assert False
    
    @pytest.mark.xfail
    def test_xfail_passes(self, record_property, caplog):
        logger.info('I passed but I should fail')
        record_property("gitlab_requirement", "5")
        assert True